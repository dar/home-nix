{
  description = "The Hive - The secretly open NixOS-Society";

  inputs.std.url = "github:divnix/std";
  inputs.std.inputs.nixpkgs.follows = "nixpkgs";
  inputs.std.inputs.devshell.follows = "devshell";
  inputs.std.inputs.nixago.follows = "nixago";
  inputs.devshell.url = "github:numtide/devshell";
  inputs.devshell.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nixago.url = "github:nix-community/nixago";
  inputs.nixago.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nixago.inputs.nixago-exts.follows = "";

  inputs.hive.url = "github:divnix/hive";
  inputs.hive.inputs.nixpkgs.follows = "nixpkgs";

  inputs.nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
  # inputs.nixpkgs.url = "github:numtide/nixpkgs-unfree/nixos-unstable";
  # inputs.nixpkgs.inputs.nixpkgs.follows = "nixpkgs-unstable";
  inputs.nixpkgs.follows = "nixos-24-11";

  # tools
  inputs = {
    nixgl.url = "github:guibou/nixGL";
    nixgl.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    impermanence.url = "github:nix-community/impermanence";
    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-generators.inputs.nixpkgs.follows = "nixpkgs";
    nixos-generators.inputs.nixlib.follows = "nixpkgs";
    colmena.url = "github:zhaofengli/colmena";
    colmena.inputs.nixpkgs.follows = "nixpkgs";
    colmena.inputs.stable.follows = "std/blank";
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";
    qnr.url = "github:divnix/quick-nix-registry";
  };
  inputs.hive.inputs = {
    colmena.follows = "colmena";
  };

  # nixpkgs & home-manager
  inputs = {
    nixos.follows = "nixos-24-11";
    home.follows = "home-24-11";

    nixos-22-05.url = "github:nixos/nixpkgs/release-22.05";
    home-22-05.url = "github:blaggacao/home-manager/release-22.05"; # some temp fixes

    nixos-22-11.url = "github:nixos/nixpkgs/release-22.11";
    home-22-11.url = "github:nix-community/home-manager/release-22.11"; # fixes incorporated

    nixos-23-05.url = "github:nixos/nixpkgs/release-23.05";
    home-23-05.url = "github:nix-community/home-manager/release-23.05";
    # home-23-05.url = "/home/dar/src/github.com/nix-community/home-manager";

    nixos-23-11.url = "github:nixos/nixpkgs/release-23.11";
    home-23-11.url = "github:nix-community/home-manager/release-23.11"; # fixes incorporated again

    nixos-24-05.url = "github:nixos/nixpkgs/release-24.05";
    home-24-05.url = "github:nix-community/home-manager/release-24.05";

    nixos-24-11.url = "github:nixos/nixpkgs/release-24.11";
    home-24-11.url = "github:nix-community/home-manager/release-24.11";
  };

  outputs = {
    std,
    hive,
    self,
    ...
  } @ inputs:
    hive.growOn {
      inherit inputs;
      nixpkgsConfig = {allowUnfree = true;};
      cellsFrom = ./comb;
      cellBlocks =
        # use blocktypes from both frameworkd
        with std.blockTypes;
        with hive.blockTypes; [
          # modules implement
          (functions "nixosModules")
          (functions "homeModules")
          (functions "devshellModules")

          # profiles activate
          (functions "hardwareProfiles")
          (functions "nixosProfiles")
          (functions "homeProfiles")
          (functions "devshellProfiles")

          # suites aggregate profiles
          (functions "nixosSuites")
          (functions "homeSuites")

          # configurations can be deployed
          nixosConfigurations
          colmenaConfigurations
          homeConfigurations
          diskoConfigurations

          # devshells can be entered
          (devshells "shells")
        ];
    }
    # soil
    {
      devShells = std.harvest self ["repo" "shells"];
    }
    {
      colmenaHive = hive.collect self "colmenaConfigurations";
      nixosConfigurations = hive.collect self "nixosConfigurations";
      homeConfigurations = hive.collect self "homeConfigurations";
      diskoConfigurations = hive.collect self "diskoConfigurations";
    };

  # --- Flake Local Nix Configuration ----------------------------
  nixConfig = {
    extra-substituters = [
      "https://numtide.cachix.org"
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };
  # --------------------------------------------------------------
}
