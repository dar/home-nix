let
  inherit (inputs.qnr.nixosModules) local-registry;
  inherit (cell.hardwareProfiles) lavinox;
  inherit (inputs) nixos-23-11;
in {
  lavinox = {
    pkgs,
    config,
    ...
  }: {
    bee.system = "x86_64-linux";
    bee.pkgs = import nixos-23-11 {
      inherit (config.bee) system;
      config.allowUnfree = true;
      overlays = [];
    };
    imports = [
      local-registry
      lavinox
    ];

    # swapDevices = [
    #   {
    #     device = "/.swapfile";
    #     size = 8192; # ~8GB - will be autocreated
    #   }
    # ];
    # Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    nix = {
      buildMachines = [
        {
          hostName = "192.168.88.250";
          sshUser = "blaggacao";
          sshKey = "/home/lar/.ssh/lar";
          system = "x86_64-linux";
          protocol = "ssh";
          # if the builder supports building for multiple architectures,
          # replace the previous line by, e.g.,
          # systems = ["x86_64-linux" "aarch64-linux"];
          maxJobs = 2;
          speedFactor = 2;
          supportedFeatures = ["nixos-test" "benchmark" "big-parallel" "kvm"];
          mandatoryFeatures = [];
        }
      ];
      distributedBuilds = true;

      gc.automatic = true;
      gc.dates = "weekly";
      optimise.automatic = true;
      nixPath = [
        "nixpkgs=${pkgs.path}"
        "nixos-config=/etc/nixos/configuration.nix"
      ];
      # qick-nix-registry options
      localRegistry = {
        # Enable quick-nix-registry
        enable = true;
        # Cache the default nix registry locally, to avoid extraneous registry updates from nix cli.
        cacheGlobalRegistry = true;
        # Set an empty global registry.
        noGlobalRegistry = false;
      };
      settings = {
        builders-use-substitutes = true;
        trusted-substituters = [
          "https://numtide.cachix.org"
          "https://nix-community.cachix.org"
        ];
        trusted-public-keys = [
          "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
        auto-optimise-store = true;
        allowed-users = ["@wheel"];
        trusted-users = ["root" "@wheel"];
        experimental-features = [
          "flakes"
          "nix-command"
        ];
        accept-flake-config = true;
      };
    };

    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Set your time zone.
    time.timeZone = "Europe/Berlin";

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    networking.useDHCP = false;
    networking.interfaces.wlp2s0.useDHCP = true;
    networking.networkmanager.enable = true;

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Select internationalisation properties.
    # i18n.defaultLocale = "en_US.UTF-8";
    # console = {
    #   font = "Lat2-Terminus16";
    #   keyMap = "us";
    # };

    # Enable the X11 windowing system.
    services.xserver = {
      enable = true;
      # Enable the GNOME Desktop Environment.
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
      # Enable touchpad support (enabled default in most desktopManager).
      libinput.enable = true;
      # Configure keymap in X11
      layout = "es";
      xkbOptions = "";
    };

    services.syncthing = {
      enable = true;
      openDefaultPorts = true;
      dataDir = "/home/lar";
      user = "lar";
      group = "users";
    };

    services.sshd.enable = true;
    # Enable CUPS to print documents.
    services.printing = {
      enable = true;
      drivers = with pkgs; [hplipWithPlugin];
    };
    # driverless printing via IPP
    services.avahi = {
      enable = true;
      nssmdns = true;
      openFirewall = true;
    };
    services.system-config-printer.enable = true;

    # Estonian EID
    services.pcscd.enable = true;
    services.pcscd.plugins = [pkgs.acsccid]; # smartcard reader from ACS
    environment.etc."chromium/native-messaging-hosts/eu.webeid.json".source = "${pkgs.web-eid-app}/share/web-eid/eu.webeid.json";
    environment.etc."opt/chrome/native-messaging-hosts/eu.webeid.json".source = "${pkgs.web-eid-app}/share/web-eid/eu.webeid.json";

    /*
    Note some websites still use PKCS#11 instead of Web eID (for Estonian ID cards). This requires different configuration.
    */

    # Tell p11-kit to load/proxy opensc-pkcs11.so, providing all available slots
    # (PIN1 for authentication/decryption, PIN2 for signing).
    environment.etc."pkcs11/modules/opensc-pkcs11".text = ''
      module: ${pkgs.opensc}/lib/opensc-pkcs11.so
    '';

    # Enable sound.
    sound.enable = true;
    sound.mediaKeys.enable = true;
    hardware.pulseaudio.enable = true;
    # Enable scanning
    hardware.sane.enable = true;

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users = {
      users.root = {
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrCl14a/OKlOzpntF6lF5/DKDSP9E8QeLZt81cciO517ViUtAtOuxluMEzd2yuzR8tIMrREQ6QAIqKtTlN/EX2OQQaN4ohqqYq3FKkU+gD03XONNGCVsRCI7tmMHq2k5rqk6dqOLLp/aj/5OsKAgop3OU9Bfx2vo5WqKo5au8bCSJE+UVdy8QeSFJ7qJ8mNXVnzzv/Epnbi4Aepglwxfw1s2brFjoRXj+qnxVFlyhQMFdAX11ZwtJ2fR9jjzkezCusJ7D/kPs1Z4+e/VcMzUk7GVFR99RjFD7jpThRgMcKYoj03zhO6XDlk+EBjZbPuWUNlmwTjhFUxl5cPwwXXnAd dar@meerkat"
        ];
      };
      users.lar = {
        shell = pkgs.zsh;
        isNormalUser = true;
        initialPassword = "password123";
        extraGroups = ["wheel"]; # Enable ‘sudo’ for the user.
      };
    };

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
      # Estonian ID
      qdigidoc
      # Wrapper script to tell to Chrome/Chromium to use p11-kit-proxy to load
      # security devices, so they can be used for TLS client auth.
      # Each user needs to run this themselves, it does not work on a system level
      # due to a bug in Chromium:
      #
      # https://bugs.chromium.org/p/chromium/issues/detail?id=16387
      (pkgs.writeShellScriptBin "setup-browser-eid" ''
        NSSDB="''${HOME}/.pki/nssdb"
        mkdir -p ''${NSSDB}
        ${pkgs.nssTools}/bin/modutil -force -dbdir sql:$NSSDB -add p11-kit-proxy \
          -libfile ${pkgs.p11-kit}/lib/p11-kit-proxy.so
      '')
      xclip
      tty-share
      alacritty
      element-desktop
      firefox
      chromium
      enpass # unfree
      # passwords
      passage # pass for age
      age
      tree # yet undeclared of passage
      # Office
      libreoffice
      onlyoffice-bin
      beancount
      beancount-black
      khoj
      fava
      direnv
      nodePackages.localtunnel
      # Git & Tools
      git
      gh
      gitoxide
      ghq
      sublime-merge # unfree
      # Nix
      # nil # nix language server
      rnix-lsp # nix language server
      alejandra # nix formatter
      # Python
      (python3Full.withPackages (p:
        with p; [
          numpy
          pandas
          ptpython
          requests
          scipy
        ]))
      poetry # python project files
      black # python formatter
      vscodium-fhs
    ];
    environment.sessionVariables = {
      PYTHONSTARTUP = let
        startup =
          pkgs.writers.writePython3 "ptpython.py"
          {
            libraries = with pkgs.python3Packages; [ptpython];
          } ''
            from __future__ import unicode_literals
            from pygments.token import Token
            from ptpython.layout import CompletionVisualisation
            import sys
            ${builtins.readFile ./ptconfig.py}
            try:
                from ptpython.repl import embed
            except ImportError:
                print("ptpython is not available: falling back to standard prompt")
            else:
                sys.exit(embed(globals(), locals(), configure=configure))
          '';
      in "${startup}";
    };

    # Programs configuration
    programs.starship.enable = true;
    programs.nix-ld.enable = true; # quality of life for downloaded programs
    programs.gnupg.agent.enable = true;
    programs.zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestions.enable = true;
      autosuggestions.async = true;
      syntaxHighlighting.enable = true;
      shellInit = ''
        eval "$(direnv hook zsh)"
      '';
    };
    programs.git = {
      enable = true;
      config = {
        user.name = "Lina Avendaño";
        user.email = "lina8823@gmail.com";
        init.defaultBranch = "main";
        core.autocrlf = "input";
        pull.rebase = true;
        rebase.autosquash = true;
        rerere.enable = true;
      };
    };
    programs.ssh = {
      extraConfig = ''
        Host github.com
          User git
          Hostname github.com
          IdentityFile ~/.ssh/lar
        Host gitlab.com
          PreferredAuthentications publickey
          IdentityFile ~/.ssh/lar
      '';
    };
    programs.chromium = {
      enable = true;
      extensions = [
        # UID of extension
        "kmcfomidfpdkfieipokbalgegidffkal" # EnPass
        "ncibgoaomkmdpilpocfeponihegamlic" # WebID EU
      ];
    };

    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ ... ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # networking.firewall.enable = false;

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "22.05"; # Did you read the comment?
  };
}
