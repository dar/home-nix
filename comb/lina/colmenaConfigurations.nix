{
  lavinox = {
    networking.hostName = "lavinox";
    deployment = {
      allowLocalDeployment = true;
      targetHost = "lavinox";
    };
    imports = [cell.nixosConfigurations.lavinox];
  };
}
