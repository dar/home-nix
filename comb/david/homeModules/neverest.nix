{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  # aliases
  inherit (config.programs) neverest;
  tomlFormat = pkgs.formats.toml {};

  # attrs util that removes entries containing a null value
  compactAttrs = filterAttrs (_: val: !isNull val);

  # needed for notmuch config, because the DB is here, and not in each
  # account's dir
  maildirBasePath = config.accounts.email.maildirBasePath;

  # make encryption config based on the given home-manager email
  # account TLS config
  mkEncryptionConfig = tls:
    if tls.useStartTls
    then "start-tls"
    else if tls.enable
    then "tls"
    else "none";

  # make a neverest account config based on the given home-manager
  # email account config
  mkAccountConfig = _: account: let
    notmuchEnabled = account.notmuch.enable;
    imapEnabled = !isNull account.imap;
    maildirEnabled =
      !isNull account.maildir
      && !notmuchEnabled;

    globalConfig = {
      default = account.primary;
    };

    imapConfig = optionalAttrs imapEnabled (compactAttrs {
      right.backend.type = "imap";
      right.backend.host = account.imap.host;
      right.backend.port = account.imap.port;
      right.backend.encryption = mkEncryptionConfig account.imap.tls;
      right.backend.login = account.userName;
      right.backend.auth.type = "password";
      right.backend.auth.cmd =
        builtins.concatStringsSep " " account.passwordCommand;
      right.folder.aliases = {
        inbox = account.folders.inbox;
        sent = account.folders.sent;
        drafts = account.folders.drafts;
        trash = account.folders.trash;
      };
    });

    maildirConfig = optionalAttrs maildirEnabled (compactAttrs {
      left.backend.type = "maildir";
      left.backend.root-dir = account.maildir.absPath;
      left.folder.aliases = {
        inbox = account.folders.inbox;
        sent = account.folders.sent;
        drafts = account.folders.drafts;
        trash = account.folders.trash;
      };
    });

    notmuchConfig = optionalAttrs notmuchEnabled (compactAttrs {
      left.backend.type = "notmuch";
      left.backend.db-path = maildirBasePath;
      left.folder.aliases = {
        inbox = account.folders.inbox;
        sent = account.folders.sent;
        drafts = account.folders.drafts;
        trash = account.folders.trash;
      };
    });

    config = attrsets.mergeAttrsList [
      globalConfig
      imapConfig
      maildirConfig
      notmuchConfig
    ];
  in
    recursiveUpdate config account.neverest.settings;
in {
  meta.maintainers = with hm.maintainers; [soywod];

  options = {
    programs.neverest = {
      enable = mkEnableOption "CLI to manage emails";
      package = mkPackageOption pkgs "neverest" {};
      settings = mkOption {
        type = types.submodule {freeformType = tomlFormat.type;};
        default = {};
        description = ''
          neverest CLI global configuration.
          See <https://github.com/pimalaya/neverest/blob/master/config.sample.toml> for supported values.
        '';
      };
    };

    accounts.email.accounts = mkOption {
      type = types.attrsOf (types.submodule {
        options.neverest = {
          enable =
            mkEnableOption
            "the email client neverest CLI for this email account";

          settings = mkOption {
            type = types.submodule {freeformType = tomlFormat.type;};
            default = {};
            description = ''
              neverest CLI configuration for this email account.
              See <https://github.com/pimalaya/neverest/blob/master/config.sample.toml> for supported values.
            '';
          };
        };
      });
    };
  };

  config = mkIf neverest.enable {
    home.packages = [neverest.package];

    xdg = {
      configFile."neverest/config.toml".source = let
        enabledAccounts =
          filterAttrs (_: account: account.neverest.enable)
          config.accounts.email.accounts;
        accountsConfig = mapAttrs mkAccountConfig enabledAccounts;
        globalConfig = compactAttrs neverest.settings;
        allConfig = globalConfig // {accounts = accountsConfig;};
      in
        tomlFormat.generate "neverest.config.toml" allConfig;

      desktopEntries.neverest = mkIf pkgs.stdenv.hostPlatform.isLinux {
        type = "Application";
        name = "neverest";
        genericName = "Email Syncer";
        comment = "CLI to manage emails";
        terminal = true;
        exec = "neverest %u";
        categories = ["Network"];
        settings = {Keywords = "email";};
      };
    };
  };
}
