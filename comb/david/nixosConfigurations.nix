# TODO:
/*

Backup:
- Wireguard config & keys
root@dar:~# ls -la /etc/wireguard/
total 32
drwx------   2 root root  4096 Jan 16 00:06 .
drwxr-xr-x 190 root root 12288 Apr 27  2024 ..
-rw-------   1 root root    45 Apr 23  2023 private.key
-rw-r--r--   1 root root    45 Apr 23  2023 public.key
-rw-------   1 root root   570 Jul 10  2024 wg0.conf
-rw-------   1 root root   257 Jan 16 00:06 wg1.conf
- SSH keys (for convenience)
- gpg keys (pass root)
- age key (passage root)
❯ echo $PASSAGE_DIR  -  $PASSAGE_EXTENSION_DIR  -  $PASSAGE_IDENTITIES_FILE
/home/dar/.local/share/passage - - /home/dar/.config/passage-identities
❯ la .ssh
Permissions Size User      Date Modified Name
.rw-rw-r--     0 blaggacao 15 Sep  2020  .known_hosts.lock
.rw-rw-r--   890 blaggacao 16 Dec  2023  authorized_keys
.rw-r--r--  1.1k blaggacao 10 Jul  2024  config
.rw-r--r--    98 blaggacao 20 Dec  2021  id_ed25519_test.pub
.r--------  1.7k blaggacao 19 Apr  2018  id_rsa
.rwxrwxrwx   393 blaggacao  5 Feb  2020  id_rsa.pub
.rw-------   399 blaggacao 29 Jun  2020  keybase-signed-key--
.rw-------   760 blaggacao 29 Jun  2020  keybase-signed-key---cert.pub
.rw-r--r--    95 blaggacao 29 Jun  2020  keybase-signed-key--.pub
.rw-------   72k blaggacao 21 Jul  2024  known_hosts
.rw-------   71k blaggacao 17 May  2024  known_hosts.old
.rw-------   411 blaggacao  8 Mar  2023  my-nixbuild-key
.rw-r--r--    96 blaggacao  8 Mar  2023  my-nixbuild-key.pub
*/
let
  inherit (inputs.qnr.nixosModules) local-registry;
  inherit (cell.hardwareProfiles) meerkat;
in {
  meerkat = {
    pkgs,
    config,
    ...
  }: {
    bee.system = "x86_64-linux";
    bee.pkgs = import inputs.nixos {
      inherit (config.bee) system;
      config.allowUnfree = true;
      overlays = [
        (import ./overlays)
      ];
    };
    imports = [
      local-registry
      meerkat
    ];

    fonts.packages = [pkgs.nerdfonts];

    # swapDevices = [
    #   {
    #     device = "/.swapfile";
    #     size = 8192; # ~8GB - will be autocreated
    #   }
    # ];
    # Use the systemd-boot EFI boot loader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.supportedFilesystems = ["ntfs"];
    nix = {
      gc.automatic = true;
      gc.dates = "weekly";
      optimise.automatic = true;
      nixPath = [
        "nixpkgs=${pkgs.path}"
        "nixos-config=/etc/nixos/configuration.nix"
      ];
      # qick-nix-registry options
      localRegistry = {
        # Enable quick-nix-registry
        enable = true;
        # Cache the default nix registry locally, to avoid extraneous registry updates from nix cli.
        cacheGlobalRegistry = true;
        # Set an empty global registry.
        noGlobalRegistry = true;
      };
      settings = {
        builders-use-substitutes = true;
        trusted-substituters = [
          "https://numtide.cachix.org"
          "https://nix-community.cachix.org"
        ];
        trusted-public-keys = [
          "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
        auto-optimise-store = true;
        allowed-users = ["@wheel"];
        trusted-users = ["root" "@wheel"];
        experimental-features = [
          "flakes"
          "nix-command"
        ];
        accept-flake-config = true;
      };
    };

    programs.nh = {
      enable = true;
      clean.enable = true;
      clean.extraArgs = "--keep-since 4d --keep 3";
      flake = "/home/dar/src/gitlab.com/dar/home-nix";
    };

    # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Set your time zone.
    time.timeZone = "Europe/Berlin";

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    networking.useDHCP = false;
    networking.interfaces.wlp58s0.useDHCP = true;
    networking.networkmanager.enable = true;

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Select internationalisation properties.
    # i18n.defaultLocale = "en_US.UTF-8";
    # console = {
    #   font = "Lat2-Terminus16";
    #   keyMap = "us";
    # };

    virtualisation.containers.enable = true;
    virtualisation = {
      podman = {
        enable = true;
        dockerCompat = true;
        # Required for containers under podman-compose to be able to talk to each other
        defaultNetwork.settings.dns_enabled = true;
      };
    };
    virtualisation.libvirtd.enable = true;

    # Enable the X11 windowing system.
    services.xserver = {
      enable = false;
    };

    # yubikey
    services.udev.packages = [pkgs.yubikey-personalization];
    services.pcscd.enable = true;
    services.pcscd.plugins = [pkgs.acsccid];
    security.pam.services = {
      # login.u2fAuth = true;
      sudo.u2fAuth = true;
    };
    security.pam.u2f = {
      enable = true;
      settings = {
        interactive = true;
        cue = false;
        appid = "pam://yubi";
        origin = "pam://yubi";
        authFile = pkgs.writeText "u2f-mappings" (pkgs.lib.concatStrings [
          "dar"
          ":Caw5yRNTghSDwSxDd8Jooi9ld/xbaxely0wjjhkieCRqrdDq9bn1MGyNPbOOA4utuwldsino6RV4+PQ1l1i8hQ==,UnawFkdJR0Ac9+bqSFC5pO0lPjV1YXIYFFDO6BO1J5n4zn2H5Q5+ayZLljmZQt4XlIE5alRxyH1NlZP+CdwIfA==,es256,+presence"
        ]);
      };
    };

    services.pipewire.enable = true;
    services.pipewire.audio.enable = true;

    # services.syncthing = {
    #   enable = true;
    #   openDefaultPorts = true;
    #   dataDir = "/home/dar";
    #   user = "dar";
    #   group = "users";
    # };

    services.sshd.enable = true;
    # Enable CUPS to print documents.
    services.printing = {
      enable = true;
      drivers = with pkgs; [hplipWithPlugin];
    };
    # driverless printing via IPP
    services.avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
    services.system-config-printer.enable = true;

    # hardware.pulseaudio.enable = true;
    # Enable scanning
    hardware.sane.enable = true;
    hardware.sane.extraBackends = [
      pkgs.hplipWithPlugin
      pkgs.sane-airscan
    ];
    hardware.bluetooth.enable = true;
    hardware.system76.enableAll = true;

    # Define a user account. Don't forget to set a password with ‘passwd’.
    users = {
      users.root = {
      };
      users.dar = {
        shell = pkgs.zsh;
        isNormalUser = true;
        initialPassword = "password123";
        extraGroups = [
          "wheel" # Enable ‘sudo’ for the user.
          "libvirtd"
        ];
      };
    };

    # List packages installed in system profile. To search, run:
    # $ nix search wget
    environment.systemPackages = with pkgs; [
      grim # screenshot functionality
      slurp # screenshot functionality
      sway-contrib.grimshot # screenshot utility using slurp + grim
      swaywsr # https://github.com/pedroscaff/swaywsr
      wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
      mako # notification system developed by swaywm maintainer
      fuzzel # sway app switcher by maker of foot
      git
      direnv
      # also in home env
      helix
      htop
      fd
      ripgrep
      rlwrap
      duf # modern df
      du-dust # modern du
      yazi

      dive # look into docker image layers
      podman-tui # status of containers in the terminal
      podman-compose # start group of containers for dev
      skopeo # Interact with container registry

      gpg-tui
    ];

    # Enable the gnome-keyring secrets vault.
    # Will be exposed through DBus to programs willing to store secrets.
    services.gnome.gnome-keyring.enable = true;

    # enable foot terminal emulator
    programs.foot = {
      enable = true;
    };

    # enable yazi file manager
    programs.yazi = {
      enable = true;
    };

    # enable sway window manager
    programs.sway = {
      enable = true;
      wrapperFeatures.gtk = true;
    };

    # enable waybar status bar
    programs.waybar = {
      enable = false;
    };

    # most notably creates a [Desktop Entry] c/o services.displayManager.sessionPackages
    programs.uwsm = {
      enable = true;
      waylandCompositors = {
        sway = {
          prettyName = "Sway";
          comment = "Sway compositor managed by UWSM";
          binPath = "/run/current-system/sw/bin/sway";
        };
      };
    };
    # Optional, hint Electron apps to use Wayland:
    environment.sessionVariables.NIXOS_OZONE_WL = "1";

    # notably aliases display-manager.service from services.displayManager - and so discovers desktop entries
    services.greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --remember --remember-session --time";
          user = "greeter";
        };
      };
    };

    # Programs configuration
    programs.starship.enable = true;
    programs.nix-ld.enable = true; # quality of life for downloaded programs
    programs.gnupg = {
      agent = {
        enable = true;
        enableSSHSupport = true;
        enableBrowserSocket = true;
      };
      dirmngr.enable = true;
    };
    hardware.gpgSmartcards.enable = true;
    programs.zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestions.enable = true;
      autosuggestions.async = true;
      syntaxHighlighting.enable = true;
      shellInit = ''
        eval "$(direnv hook zsh)"
      '';
    };

    # This value determines the NixOS release from which the default
    # settings for stateful data, like file locations and database versions
    # on your system were taken. It‘s perfectly fine and recommended to leave
    # this value at the release version of the first install of this system.
    # Before changing this value read the documentation for this option
    # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
    system.stateVersion = "24.11"; # Did you read the comment?
  };
}
