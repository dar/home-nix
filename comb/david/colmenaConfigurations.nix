{
  meerkat = {
    networking.hostName = "meerkat";
    deployment = {
      allowLocalDeployment = true;
      targetHost = "meerkat";
    };
    imports = [cell.nixosConfigurations.meerkat];
  };
}
