let
  inherit (config._module.args) pkgs;
in
  with pkgs; [
    element-desktop
    # chrysalis
    inkscape
    gimp
    ausweisapp
  ]
