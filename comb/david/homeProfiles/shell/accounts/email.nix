{
  # certificatesFile = "/etc/ssl/certs/ca-certificates.crt";
  # maildirBasePath = "Maildir";
  accounts = {
    "xoe/david" = {
      address = "david@xoe.dev";
      userName = "david@xoe.dev";
      realName = "David Arnold";
      primary = true;
      folders = {
        inbox = "INBOX";
      };
      himalaya = {
        enable = true;
      };
      neverest = {
        enable = true;
      };
      aliases = [
        "admin@xoe.dev"
      ];
      gpg = {
        key = "0xE9EA6EA589FD3937";
        signByDefault = true;
      };
      passwordCommand = "pass david@xoe.dev/david@xoe.dev"; # currently not supported by thunderbird
      # signature = {
      #   showSignature = "append";
      #   command = "pass david@xoe.dev/signature"; # currently ony supported by neomutt
      # };
      smtp = {
        host = "smtp.migadu.com";
        port = 465;
      };
      imap = {
        host = "imap.migadu.com";
        port = 993;
      };
    };
    "dgx" = {
      address = "dgx.arnold@gmail.com";
      userName = "dgx.arnold@gmail.com";
      realName = "David Arnold";
      primary = false;
      flavor = "gmail.com";
      folders = {
        inbox = "INBOX";
      };
      himalaya = {
        enable = true;
      };
      neverest = {
        enable = true;
      };
      gpg = {
        key = "0xE9EA6EA589FD3937";
        signByDefault = true;
      };
      passwordCommand = "pass google.com/dgx.arnold@gmail.com-apppassword";
      # signature = {
      #   showSignature = "append";
      #   command = "pass david@xoe.dev/signature"; # currently ony supported by neomutt
      # };
    };
  };
}
