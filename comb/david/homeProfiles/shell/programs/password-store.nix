let
  inherit (config._module.args) pkgs;
in {
  enable = true;
  package = pkgs.symlinkJoin {
    name = "pass";
    paths = [pkgs.pass-wayland pkgs.passage];
  };
  settings = {
    PASSWORD_STORE_DIR = "${config.xdg.dataHome}/password-store";
    PASSAGE_DIR = "${config.xdg.dataHome}/passage";
    PASSAGE_IDENTITIES_FILE = "${config.xdg.configHome}/passage-identities";
    PASSAGE_EXTENSIONS_DIR = "${config.xdg.dataHome}/passage-extensions";
  };
}
