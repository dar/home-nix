let
  inherit (inputs.nixpkgs.lib) importTOML recursiveUpdate;
  inherit (builtins) mapAttrs isString;
in {
  enable = true;
  languages = {
    language-server.lemminx = {
      command = "lemminx";
    };
    language-server.beancount-language-server = {
      command = "beancount-language-server";
      config = {journal_file = "/home/dar/src/gitlab.com/dar/count/main.beancount";};
      args = ["--stdio"];
    };
    language-server.ltex = {
      command = "ltex-ls";
      config.ltex = {
        dictionary = {en-US = [];};
        disabledRules = {en-US = ["PROFANITY"];};
      };
    };
    language = [
      {
        name = "beancount";
        language-servers = ["beancount-language-server"];
      }
      {
        name = "nix";
        formatter = {command = "alejandra";};
        auto-format = true;
      }
      {
        language-servers = ["marksman" "ltex"];
        name = "markdown";
      }
      {
        name = "xml";
        file-types = ["xsl" "xml"];
        language-servers = ["lemminx"];
      }
    ];
  };
  # https://docs.helix-editor.com/configuration.html
  settings = {
    theme = "onedark";
    editor = {
      middle-click-paste = false;
      line-number = "relative";
      scroll-lines = 5;
      cursorline = true;
      bufferline = "multiple";
      color-modes = true;
      statusline = {
        center = ["version-control"];
      };
      # https://docs.helix-editor.com/configuration.html#editorlsp-section
      lsp = {
        display-inlay-hints = false; # being improved on in helix
      };
      # https://docs.helix-editor.com/configuration.html#editorfile-picker-section
      file-picker = {};
      whitespace = {
        tab = "all";
        nbsp = "all";
        characters = {
          tabpad = ".";
        };
      };
      indent-guides = {
        render = true;
        character = "⸽";
        skip-levels = 2;
      };
      soft-wrap = {
        enable = true;
      };
    };
    keys = let
      _keys = importTOML ./keys.normal.toml;
      mapping_table = {
        # searching for all command having a correspondence with extend_ prefix
        # https://github.com/helix-editor/helix/blob/master/helix-term/src/commands.rs
        # see also: https://docs.helix-editor.com/keymap.html#select--extend-mode
        # > Select mode echoes Normal mode, but changes any movements to extend
        # > selections rather than replace them. Goto motions are also changed to extend [...]
        # goto commands seems to have a different handling
        "move_char_left" = "extend_char_left";
        "move_char_right" = "extend_char_right";
        "move_line_down" = "extend_line_down";
        "move_line_up" = "extend_line_up";
        "move_visual_line_down" = "extend_line_down";
        "move_visual_line_up" = "extend_line_up";
        "move_prev_word_start" = "extend_prev_word_start";
        "move_next_word_end" = "extend_next_word_end";
        "move_prev_long_word_start" = "extend_prev_long_word_start";
        "move_next_long_word_end" = "extend_next_long_word_end";
        "move_parent_node_start" = "extend_parent_node_start";
        "move_parent_node_end" = "extend_parent_node_end";
        "find_till_char" = "extend_till_char";
        "find_next_char" = "extend_next_char";
        "till_prev_char" = "extend_till_prev_char";
        "find_prev_char" = "extend_prev_char";
        "search_next" = "extend_search_prev";
        "search_prev" = "extend_search_prev";
      };
    in
      recursiveUpdate _keys {
        # remap to sticky view, too
        normal.Z = _keys.normal.z;
        select =
          mapAttrs (_: v:
            if isString v
            then mapping_table.${v} or v
            else v)
          _keys.normal
          // {
            # rebound select mode key in normal mode -> override "append" in select mode
            "a" = "no_op";
            "C-a" = "no_op";
          };
      };
  };
}
