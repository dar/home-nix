{
  enable = true;
  settings = {
    ################################################################################
    #### Preset configuration ######################################################
    ################################################################################

    presets.default = {
      # TCP configuration, used by server binders and clients. Requires the
      # cargo feature "tcp".
      #
      tcp.host = "localhost";
      tcp.port = 9999;

      # A cycle is a step in the timer lifetime, represented by a name and a
      # duration. You can either define custom cycles:
      #
      # cycles = [
      #   { name = "Work", duration = 5 },
      #   { name = "Rest", duration = 3 },
      # ]

      # Predefined cycles can also be used:
      #
      preset = "52/17"; #| "pomodoro"

      # Force the timer to stop after the given amount of loops:
      #
      #cycles-count = 5

      # Customize the timer precision. Available options: second, minute, hour.
      #
      #timer-precision = "minute"

      # A hook can be either a shell command or a system notification. Hook
      # names follow the format "on-{name}-{event}", where "name" is the
      # kebab-case version of the cycle name, and "event" the type of event:
      # begin, running, set, pause, resume, end.
      #
      hooks.on-work-begin.notify.summary = "Comodoro";
      hooks.on-work-begin.notify.body = "Work for 52 min!";
      hooks.on-work-pause.notify.summary = "Comodoro";
      hooks.on-work-pause.notify.body = "Work paused!";
      hooks.on-work-resume.notify.summary = "Comodoro";
      hooks.on-work-resume.notify.body = "Work resumed!";

      hooks.on-rest-begin.notify.summary = "Comodoro";
      hooks.on-rest-begin.notify.body = "Rest for 17 min!";
      hooks.on-rest-pause.notify.summary = "Comodoro";
      hooks.on-rest-pause.notify.body = "Rest paused!";
      hooks.on-rest-resume.notify.summary = "Comodoro";
      hooks.on-rest-resume.notify.body = "Rest resumed!";
    };
  };
}
