let
  inherit (config._module.args) pkgs;
in
  with pkgs; [
    nix
    choose # between cut & awk - beautiful
    curlie # modern curl
    dogdns # modern dig
    duf # modern df
    du-dust # modern du
    eza # modern ls (not on LSD)
    fx # jq, but don't admit you somtimes like to use a mouse
    fd # modern find
    ripgrep
    gping # modern ping
    hyperfine # benchmark shell commands like a boss
    ijq # interactive jq wrapper, requires jq
    # magic-wormhole # secure file sharing over cli
    procs # modern ps
    sd # modern sed
    thefuck # if you mistyped: fuck
    tty-share # Secure terminal-session sharing
    watchexec # Executes commands in response to file modifications
    h # faster shell navigation of projects
    jd-diff-patch # semantic json differ
    arping
    eva # modern bc
    manix # explore nixos/hm options
    borgbackup # backup tool
    git-filter-repo # rewrite git history like a pro (and fast)
    pulsemixer # tui for pulseaudio
    icdiff # better diff
    nix-tree # tui to explore the nix closure
    sublime-merge
    # office
    invoice2data
    beancount
    nb # note taking
    # language servers
    nil # for nix
    nls # for nickel
    nodePackages.bash-language-server
    nodePackages.typescript-language-server
    vscode-langservers-extracted
    nodePackages.vls # for vue
    (python311.withPackages (ps:
      with ps; [
        python-lsp-server
        python-lsp-ruff
        pylsp-rope
      ]))
    beancount-language-server
    marksman # markdown
    emmet-ls
    rlwrap # readline wrap
    ltex-ls # https://github.com/valentjn/ltex-ls
    pipx
    pavucontrol # waybar on click

    neverest # TODO: move to hm config once available
    cardamum # TODO: move to hm config once available

    zenity # dialogues for TUIs (matui)
    ffmpeg-full
  ]
