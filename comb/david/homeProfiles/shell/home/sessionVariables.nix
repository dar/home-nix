let
  inherit (config._module.args) pkgs;
in {
  MANPAGER = "bat -p -lman";
  PATH = "/home/dar/.local/bin:$PATH";
  LD_LIBRARY_PATH = "$LD_LIBRARY_PATH:${pkgs.stdenv.cc.cc.lib}/lib";
}
