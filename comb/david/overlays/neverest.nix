# TODO: move this to nixpkgs
# This file aims to be an up-to-date replacement on master for the nixpkgs derivation.
{
  lib,
  pkg-config,
  rustPlatform,
  fetchFromGitHub,
  buildPackages,
  stdenv,
  apple-sdk,
  installShellFiles,
  installShellCompletions ? stdenv.buildPlatform.canExecute stdenv.hostPlatform,
  installManPages ? stdenv.buildPlatform.canExecute stdenv.hostPlatform,
  notmuch,
  withNoDefaultFeatures ? false,
  withFeatures ? [],
}: let
  version = "1.0.0-rc-2024-12-19";
  hash = "sha256-MXQDy5b/ooSkISEVM9KxHeqkkhPimZJtX3j9jqTP5aU=";
  cargoHash = "sha256-oQrbHYbsfPAVAe3UPawRg/GRTIytQZMcxZcwC8J1T/Y=";
in
  rustPlatform.buildRustPackage {
    inherit version cargoHash;

    pname = "neverest";

    src = fetchFromGitHub {
      inherit hash;
      owner = "pimalaya";
      repo = "neverest";
      # rev = "v${version}";
      rev = "cc5f5214d3bea064ed059116ac81e40a803faa7e";
    };

    useFetchCargoVendor = true;

    buildNoDefaultFeatures = withNoDefaultFeatures;
    buildFeatures = withFeatures;

    nativeBuildInputs =
      [pkg-config]
      ++ lib.optional (installManPages || installShellCompletions) installShellFiles;

    buildInputs =
      []
      ++ lib.optional stdenv.hostPlatform.isDarwin apple-sdk
      ++ lib.optional (builtins.elem "notmuch" withFeatures) notmuch;

    doCheck = false;
    auditable = false;

    # unit tests only
    cargoTestFlags = ["--lib"];

    postInstall = let
      emulator = stdenv.hostPlatform.emulator buildPackages;
    in
      ''
        mkdir -p $out/share/{completions,man}
        ${emulator} "$out"/bin/neverest man "$out"/share/man
        ${emulator} "$out"/bin/neverest completion bash > "$out"/share/completions/neverest.bash
        ${emulator} "$out"/bin/neverest completion elvish > "$out"/share/completions/neverest.elvish
        ${emulator} "$out"/bin/neverest completion fish > "$out"/share/completions/neverest.fish
        ${emulator} "$out"/bin/neverest completion powershell > "$out"/share/completions/neverest.powershell
        ${emulator} "$out"/bin/neverest completion zsh > "$out"/share/completions/neverest.zsh
      ''
      + lib.optionalString installManPages ''
        installManPage "$out"/share/man/*
      ''
      + lib.optionalString installShellCompletions ''
        installShellCompletion "$out"/share/completions/neverest.{bash,fish,zsh}
      '';

    meta = with lib; {
      description = "CLI to manage emails";
      mainProgram = "neverest";
      homepage = "https://github.com/pimalaya/neverest";
      changelog = "https://github.com/pimalaya/neverest/blob/v${version}/CHANGELOG.md";
      license = licenses.mit;
      maintainers = with maintainers; [soywod];
    };
  }
