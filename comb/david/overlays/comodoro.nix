# TODO: move this to nixpkgs
# This file aims to be a replacement for the nixpkgs derivation.
{
  lib,
  pkg-config,
  rustPlatform,
  fetchFromGitHub,
  stdenv,
  buildPackages,
  apple-sdk,
  installShellFiles,
  installShellCompletions ? stdenv.buildPlatform.canExecute stdenv.hostPlatform,
  installManPages ? stdenv.buildPlatform.canExecute stdenv.hostPlatform,
  withNoDefaultFeatures ? false,
  withFeatures ? [],
}: let
  version = "1.0.0-rc-2024-11-29";
  hash = "sha256-mp115TbUc226kDWDXtrbLO5sc/UqVFSNbqQBOnf7AFQ=";
  cargoHash = "sha256-NwEKPhZq6eLm1kgM6KLgDnXBIebMyi3bpIEoM/kjkho=";
in
  rustPlatform.buildRustPackage rec {
    inherit cargoHash version;

    pname = "comodoro";

    src = fetchFromGitHub {
      inherit hash;
      # rev = "v${version}";
      rev = "997de49cd70dcc488c5a1e16475e4ca8f03485f7";
      owner = "pimalaya";
      repo = "comodoro";
    };

    useFetchCargoVendor = true;

    buildNoDefaultFeatures = withNoDefaultFeatures;
    buildFeatures = withFeatures;

    nativeBuildInputs =
      [pkg-config]
      ++ lib.optional (installManPages || installShellCompletions) installShellFiles;

    buildInputs = lib.optional stdenv.hostPlatform.isDarwin apple-sdk;

    doCheck = false;
    auditable = false;

    # unit tests only
    cargoTestFlags = ["--lib"];

    postInstall = let
      emulator = stdenv.hostPlatform.emulator buildPackages;
    in
      ''
        mkdir -p $out/share/{completions,man}
        ${emulator} "$out"/bin/comodoro man "$out"/share/man
        ${emulator} "$out"/bin/comodoro completion bash > "$out"/share/completions/comodoro.bash
        ${emulator} "$out"/bin/comodoro completion elvish > "$out"/share/completions/comodoro.elvish
        ${emulator} "$out"/bin/comodoro completion fish > "$out"/share/completions/comodoro.fish
        ${emulator} "$out"/bin/comodoro completion powershell > "$out"/share/completions/comodoro.powershell
        ${emulator} "$out"/bin/comodoro completion zsh > "$out"/share/completions/comodoro.zsh
      ''
      + lib.optionalString installManPages ''
        installManPage "$out"/share/man/*
      ''
      + lib.optionalString installShellCompletions ''
        installShellCompletion "$out"/share/completions/comodoro.{bash,fish,zsh}
      '';

    meta = with lib; {
      description = "CLI to manage emails";
      mainProgram = "comodoro";
      homepage = "https://github.com/pimalaya/comodoro";
      changelog = "https://github.com/pimalaya/comodoro/blob/v${version}/CHANGELOG.md";
      license = licenses.mit;
      maintainers = with maintainers; [soywod];
    };
  }
