final: prev: {
  himalaya = prev.callPackage ./himalaya.nix {};
  neverest = prev.callPackage ./neverest.nix {};
  comodoro = prev.callPackage ./comodoro.nix {};
  cardamum = prev.callPackage ./cardamum.nix {};
  helix = prev.callPackage ./helix.nix {};
}
