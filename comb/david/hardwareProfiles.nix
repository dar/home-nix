{
  meerkat = {
    pkgs,
    config,
    lib,
    modulesPath,
    ...
  }: {
    imports = [
      (modulesPath + "/installer/scan/not-detected.nix")
      inputs.disko.nixosModules.disko
      cell.diskoConfigurations.meerkat
    ];

    # swapDevices = [
    #   {device = "/dev/disk/by-uuid/a65ded1e-741c-4a8c-9a72-4a2a7db45668";}
    # ];

    boot.initrd.availableKernelModules = ["xhci_pci" "ahci" "nvme" "usbhid" "rtsx_pci_sdmmc"];
    boot.initrd.kernelModules = ["dm-snapshot"];
    boot.kernelModules = ["kvm-intel"];
    boot.extraModulePackages = [];

    # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
    # (the default) this is the recommended approach. When using systemd-networkd it's
    # still possible to use this option, but it's recommended to use it in conjunction
    # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
    networking.useDHCP = lib.mkDefault true;
    # networking.interfaces.docker0.useDHCP = lib.mkDefault true;
    # networking.interfaces.eno1.useDHCP = lib.mkDefault true;
    # networking.interfaces.enx1c869a39459f.useDHCP = lib.mkDefault true;
    # networking.interfaces.wg1.useDHCP = lib.mkDefault true;
    # networking.interfaces.wlp58s0.useDHCP = lib.mkDefault true;

    powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
    hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

    hardware.enableRedistributableFirmware = true;
    hardware.enableAllFirmware = true;
  };
}
