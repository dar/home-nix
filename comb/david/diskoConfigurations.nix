{
  meerkat = {
    imports = [inputs.impermanence.nixosModules.impermanence];

    /*
    * https://grahamc.com/blog/erase-your-darlings/ *
    */

    environment.persistence."/persist" = {
      hideMounts = true;
      directories = [
        # "/var/log"
        "/var/lib/bluetooth"
        "/var/lib/nixos"
        # "/var/lib/systemd/coredump"
        "/etc/NetworkManager/system-connections"
        "/etc/wireguard"
        "/etc/ssh"
        # { directory = "/var/lib/colord"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o="; }
      ];
      files = [
        "/etc/machine-id"
        # { file = "/etc/nix/id_rsa"; parentDirectory = { mode = "u=rwx,g=,o="; }; }
      ];
    };
    fileSystems."/persist".neededForBoot = true;
    networking.hostId = "0dfac8ea";

    disko.devices = {
      disk = {
        main = {
          type = "disk";
          device = "/dev/nvme0n1";
          content = {
            type = "gpt";
            partitions = {
              ESP = {
                type = "EF00";
                priority = 1;
                start = "1MiB";
                end = "1GB";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot";
                  mountOptions = ["umask=0077"];
                };
              };
              zfs = {
                priority = 2;
                size = "100%";
                content = {
                  type = "zfs";
                  pool = "zroot";
                };
              };
            };
          };
        };
      };
      zpool = {
        zroot = {
          type = "zpool";
          # mode = "";
          # Workaround: cannot import 'zroot': I/O error in disko tests
          options.cachefile = "none";
          rootFsOptions = {
            compression = "on";
            "com.sun:auto-snapshot" = "false";
          };
          mountpoint = "/";
          postCreateHook = "zfs list -t snapshot -H -o name | grep -E '^zroot@blank$' || zfs snapshot zroot@blank";

          datasets = {
            "local/nix" = {
              type = "zfs_fs";
              mountpoint = "/nix";
              options = {
                atime = "off";
                # "com.sun:auto-snapshot" = "true";
              };
            };
            "safe/home" = {
              type = "zfs_fs";
              mountpoint = "/home";
              options = {
                # "com.sun:auto-snapshot" = "true";
              };
            };
            "safe/persist" = {
              type = "zfs_fs";
              mountpoint = "/persist";
              options = {
                # "com.sun:auto-snapshot" = "true";
              };
            };

            # Examples (https://github.com/nix-community/disko/blob/master/example/zfs.nix)

            # unmounted can't be manually mounted via `mount`
            # zfs_unmounted_fs = {
            #   type = "zfs_fs";
            #   options.mountpoint = "none";
            # };

            # legacy can be manually mounted via `mount`
            # zfs_legacy_fs = {
            #   type = "zfs_fs";
            #   options.mountpoint = "legacy";
            #   mountpoint = "/zfs_legacy_fs";
            # };

            # zfs_volume = {
            #   type = "zfs_volume";
            #   size = "10M";
            #   content = {
            #     type = "filesystem";
            #     format = "ext4";
            #     mountpoint = "/ext4onzfs";
            #   };
            # };

            # zfs_encryptedvolume = {
            #   type = "zfs_volume";
            #   size = "10M";
            #   options = {
            #     encryption = "aes-256-gcm";
            #     keyformat = "passphrase";
            #     keylocation = "file:///tmp/secret.key";
            #   };
            #   content = {
            #     type = "filesystem";
            #     format = "ext4";
            #     mountpoint = "/ext4onzfsencrypted";
            #   };
            # };

            # encrypted = {
            #   type = "zfs_fs";
            #   options = {
            #     mountpoint = "none";
            #     encryption = "aes-256-gcm";
            #     keyformat = "passphrase";
            #     keylocation = "file:///tmp/secret.key";
            #   };
            #   # use this to read the key during boot
            #   # postCreateHook = ''
            #   #   zfs set keylocation="prompt" "zroot/$name";
            #   # '';
            # };

            # "encrypted/test" = {
            #   type = "zfs_fs";
            #   mountpoint = "/zfs_crypted";
            # };
          };
        };
      };
    };
  };
}
