{
  inputs,
  cell,
}: let
  name = "David Arnold";
  email = "david@xoe.dev";
  gitSigningKey = "AB15A6AF1101390D";

  programs = {
    git = {
      userName = name;
      userEmail = email;
      signing = {
        key = gitSigningKey;
        signByDefault = true;
      };
    };
  };
  bee = rec {
    system = "x86_64-linux";
    home = inputs.home;
    pkgs = import inputs.nixos {
      inherit system;
      config = {allowUnfree = true;};
      overlays = [
        inputs.nixgl.overlay
        (import ./overlays)
      ];
    };
  };
  home = rec {
    homeDirectory = "/home/${username}";
    stateVersion = "24.11";
    username = "dar";
  };
  manual = {
    manpages.enable = false; # causes error
    html.enable = false; # saves space
    json.enable = false; # don't know what to do with this
  };
in {
  workstation = {
    inherit programs bee home manual;
    targets.genericLinux.enable = true;
    imports = with cell.homeModules;
    with cell.homeProfiles; [
      # modules
      himalaya
      neverest
      # profiles
      gui
      shell
    ];
  };
  server = {
    inherit programs bee home manual;
    targets.genericLinux.enable = true;
    imports = with cell.homeModules;
    with cell.homeProfiles; [
      # modules
      himalaya
      neverest
      # profiles
      gui
      shell
    ];
  };
}
